//
//  AppDelegate.swift
//  ArkWeiBo
//
//  Created by 金鑫 on 2020/5/29.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UITabBar.appearance().tintColor = UIColor.orange
        UINavigationBar.appearance().tintColor = UIColor.orange
        
//        window = UIWindow(frame: UIScreen.main.bounds)
//        window?.rootViewController = MainViewController();
//        window?.makeKeyAndVisible()
        
        return true
    }

}

