//
//  UIBarButtonItem-Extension.swift
//  ArkWeiBo
//
//  Created by 金鑫 on 2020/5/30.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    convenience init(imageName: String) {
        
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: imageName), for: .normal)
        btn.setImage(UIImage(named: imageName + "_highlighted"), for: .highlighted)
        btn.sizeToFit()
        
        self.init(customView: btn)
    }
    
}
