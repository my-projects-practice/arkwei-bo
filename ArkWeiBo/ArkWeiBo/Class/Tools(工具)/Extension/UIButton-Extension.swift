//
//  UIButton-Extension.swift
//  ArkWeiBo
//
//  Created by 金鑫 on 2020/5/30.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

extension UIButton {
    
    convenience init(imageName: String, bgImageName: String) {
        self.init()
        
        setBackgroundImage(UIImage(named: bgImageName), for: .normal)
        setBackgroundImage(UIImage(named: bgImageName + "_highlighted"), for: .highlighted)
        setImage(UIImage(named: imageName), for: .normal)
        setImage(UIImage(named: imageName + "_highlighted"), for: .highlighted)
        sizeToFit()
    }
}
