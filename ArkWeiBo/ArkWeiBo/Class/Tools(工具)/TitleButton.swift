//
//  TitleButton.swift
//  ArkWeiBo
//
//  Created by 金鑫 on 2020/5/30.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class TitleButton: UIButton {

   // MARK: -重写init函数
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setImage(UIImage(named: "navigationbar_arrow_down"), for: .normal)
        setImage(UIImage(named: "navigationbar_arrow_up"), for: .selected)
        setTitleColor(UIColor.black, for: .normal)
        setTitleColor(UIColor.orange, for: .selected)
        sizeToFit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel!.frame.origin.x = 0
        imageView!.frame.origin.x = titleLabel!.frame.size.width + 5
    }
}
