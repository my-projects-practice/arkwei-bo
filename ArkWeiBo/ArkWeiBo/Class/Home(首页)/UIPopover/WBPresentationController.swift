//
//  WBPresentationController.swift
//  ArkWeiBo
//
//  Created by 金鑫 on 2020/6/1.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class WBPresentationController: UIPresentationController {
    // MARK: -懒加载属性
    private lazy var coverView : UIView = UIView()
    
    // MARK: -系统回调函数
    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
        
        // 1.设置弹出View的尺寸
        presentedView?.frame = CGRect(x: 100, y: 65, width: 180, height: 250)
        
        // 2.添加蒙版
        setupCoverView()
    }
}

// MARK: -设置UI界面相关
extension WBPresentationController {
    private func setupCoverView() {
        // 1.添加蒙版
        containerView?.insertSubview(coverView, at: 0);
        
        // 2.设置蒙版的属性
        coverView.backgroundColor = UIColor(white: 0.5, alpha: 0.2)
        coverView.frame = containerView!.bounds
        
        // 3.添加手势
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(coverViewClick))
        coverView.addGestureRecognizer(tapGesture);
    }
}

// MARK:- 事件监听
extension WBPresentationController {
    @objc private func coverViewClick() {
        presentedViewController.dismiss(animated: true, completion: nil)
    }
}
