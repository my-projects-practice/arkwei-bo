//
//  HomeViewController.swift
//  ArkWeiBo
//
//  Created by 金鑫 on 2020/5/29.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    private lazy var titleBtn: TitleButton = TitleButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 1.没有登录时设置的内容
        visitorView.addRotationAnim()
        if !isLogin {
            return
        }
        
        // 2.设置导航栏的内容
        setupNavigationBar()
    }
}

// MARK: -设置UI界面
extension HomeViewController {
    
    private func setupNavigationBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(imageName: "navigationbar_friendattention")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(imageName: "navigationbar_pop")
        
        titleBtn.setTitle("请点击", for: .normal)
        titleBtn.addTarget(self, action: #selector(titleBtnClick), for: .touchUpInside)
        navigationItem.titleView = titleBtn
    }
}

// MARK: -点击事件
extension HomeViewController {
    @objc private func titleBtnClick() {
        titleBtn.isSelected = !titleBtn.isSelected
        
        let popoverVc = PopoverViewController()
        
        popoverVc.modalPresentationStyle = .custom
        
        popoverVc.transitioningDelegate = self;
        
        present(popoverVc, animated: true, completion: nil)
    }
}

extension HomeViewController : UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        // presented是发起的控制器，presenting是正在弹出的控制器
        return WBPresentationController(presentedViewController: presented, presenting: presenting)
    }
}
