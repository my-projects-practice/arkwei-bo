//
//  ProfileViewController.swift
//  ArkWeiBo
//
//  Created by 金鑫 on 2020/5/29.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        visitorView.setupVisitorViewInfo(iconName: "visitordiscover_image_profile", title: "登录后，你的微博、相册、个人资料会显示在这里，展示给别人")
    }

}
