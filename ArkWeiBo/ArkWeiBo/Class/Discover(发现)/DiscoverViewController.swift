//
//  DiscoverViewController.swift
//  ArkWeiBo
//
//  Created by 金鑫 on 2020/5/29.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class DiscoverViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        visitorView.setupVisitorViewInfo(iconName: "visitordiscover_image_message", title: "登录后，别人评论你的微博，给你发消息，都会在这里收到通知")
    }
    
}
