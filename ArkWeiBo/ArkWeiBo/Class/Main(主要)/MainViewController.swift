//
//  MainViewController.swift
//  ArkWeiBo
//
//  Created by 金鑫 on 2020/5/29.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {
    
    private lazy var composeBtn: UIButton = UIButton(imageName: "tabbar_compose_icon_add", bgImageName: "tabbar_compose_button")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupComoseBtn()
    }
}

// MARK: -设置UI界面
extension MainViewController {
    
    private func setupComoseBtn() {
        tabBar.addSubview(composeBtn)
        
        composeBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(tabBar.snp.centerX)
            make.centerY.equalTo(tabBar.bounds.size.height * 0.5)
        }
        
        composeBtn.addTarget(self, action: #selector(composeBtnClick), for: .touchUpInside)
    }
    
}

// MARK: -事件监听
extension MainViewController {
    
    @objc private func composeBtnClick() {
        print("composeBtnClick()")
    }
    
}
