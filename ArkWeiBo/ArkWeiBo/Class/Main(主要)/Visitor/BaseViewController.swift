//
//  BaseViewController.swift
//  ArkWeiBo
//
//  Created by 金鑫 on 2020/5/30.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class BaseViewController: UITableViewController {

    lazy var visitorView: VisitorView = VisitorView.visitorView()
    
    var isLogin: Bool = true
    
    override func loadView() {
        isLogin ? super.loadView() : setupVisitorView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationItems()
    }
    
}

extension BaseViewController {
    
    private func setupVisitorView() {
        view = visitorView
        
        visitorView.registerBtn.addTarget(self, action: #selector(registerBtnClick), for: .touchUpInside)
        
        visitorView.loginBtn.addTarget(self, action: #selector(loginBtnClick), for: .touchUpInside)
    }
    
    // 设置导航栏左右的Item
    private func setupNavigationItems() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: .plain, target: self, action: #selector(registerBtnClick))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: .plain, target: self, action: #selector(loginBtnClick))
    }

}

// MARK: -事件监听
extension BaseViewController {
    @objc private func registerBtnClick() {
        print("registerBtnClick")
    }
    
    @objc private func loginBtnClick() {
        print("loginBtnClick")
    }
}




